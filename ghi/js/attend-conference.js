window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading id
      const load = document.getElementById('loading-conference-spinner');
      load.classList.add('d-none');

      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }

    const successMessage = document.getElementById('success-message');
    const attendeeForm = document.getElementById('create-attendee-form');
    attendeeForm.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(attendeeForm);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        }
        const response = await fetch('http://localhost:8001/api/attendees/', fetchConfig);
        if (response.ok) {
          attendeeForm.reset();
          attendeeForm.classList.add('d-none');
          successMessage.classList.remove('d-none');
        }
        // You both sit for a moment in, if not comfortable silence, not uncomfortable silence.

    })

});
