import { NavLink } from 'react-router-dom';

function Navbar() {
    return <>
    <table>
        <tbody>
            <tr>
                <td>
                    <NavLink to="/">Home</NavLink>
                </td>
                <td>
                    <NavLink to="/attendees">Attendees</NavLink>
                </td>
                <td>
                    <NavLink to="/locations">Locations</NavLink>
                </td>
                <td>
                    <NavLink to="/conferences">Conferences</NavLink>
                </td>
                <td>
                    <NavLink to="/presentations">Presentations</NavLink>
                </td>
            </tr>
        </tbody>
    </table>
    </>
}
